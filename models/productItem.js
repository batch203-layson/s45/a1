const mongoose = require("mongoose");

const productItemSchema = new mongoose.Schema({
    productItemName: {
        type: String,
        required: [true, "Product/Item name is required"]
    },
    productItemtDescription: {
        type: String,
        required: [true, "Product/Item description is required"]
    },
    productItemPrice: {
        type: String,
        required: [true, "Product/Item price is required"]
    },
    productItemStocks: {
        type: Number,
        required: [true, "Product/Item number of stocks is required"]
    },
    productItemIsActive: {
        type: Boolean,
        default: true
    },
    productItemSetUpOn: {
        type: Date,
        default: new Date()
    },
    orderBuyers: [
        {
            userAccountId: {
                type: String,
                required: [true, "User/Account ID is required"]
            },
            userAccountEmail: {
                type: String,
                required: [true, "Email is required"]
            },
            orderQuantity: {
                type: Number,
                required: [true, "Product/Item Quantity is required"]
            },
            orderQuantity: {
                type: Number,
                required: [true, "Product/Item Quantity is required"]
            },
            orderPurchasedDate: {
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model("ProductItem", productItemSchema);
