const express = require("express");
const router = express.Router();
const userAccountControllers = require("../controllers/userAccountControllers");
const auth = require("../auth");



// Route for user registration
router.post("/register", userAccountControllers.registerUserAccount);

// Route for user authentication
 router.post("/login", userAccountControllers.loginUser);

// Route for viewing a specific user detail
router.get("/:userAccountId", userAccountControllers.getUserDetails);

/* 
// Route for viewing a specific user detail

router.post("/checkout", userAccountControllers.checkout);
 
*/










////////////////////////////////////////////////////
module.exports = router;