const UserAccount = require("../models/UserAccount");
const ProductItem = require("../models/ProductItem");
const bcrypt = require("bcrypt");
const auth = require("../auth");


// User Registration
module.exports.registerUserAccount = (req, res)=>{

    let newUser = UserAccount({

        userAccountFirstName: req.body.userAccountFirstName,
        userAccountLastName: req.body.userAccountLastName,
        userAccountEmail: req.body.userAccountEmail,
        /* 
        userAccountPassword: req.body.userAccountPassword,
         */
        userAccountPassword: bcrypt.hashSync(req.body.userAccountPassword, 10),
        userAccountMobileNumber: req.body.userAccountMobileNumber

    })

    return newUser.save()
    .then(UserAccount=>{
        res.send({message:"Registration Successful"});
    })
    .catch(error=>{
        res.send(false);
    })
}

// User Login Authentication

module.exports.loginUser = (req, res) => {
    return UserAccount.findOne({ userAccountEmail: req.body.userAccountEmail })
        .then(result => {
            // User does not exists
            if (result == null) {
                // return res.send(false);
                return res.send({ message: "User not registered yet" });
            }
            // User exists
            else {
                // Syntax: bcrypt.compareSync(data, encrypted)
                const isPasswordCorrect = bcrypt.compareSync(req.body.userAccountPassword, result.userAccountPassword);

                // If the passwords match/result of the above code is true.
                if (isPasswordCorrect) {
                    // Generate an access token
                    // Uses the "createAccessToken" method defined in the "auth.js" file
                    // Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
                    return res.send(
                        [
                            { message: "Login success" },
                            { accessToken: auth.createAccessToken(result) }
                        ]
                        );
                    
                }
                else {
                    // return false; // if password do not match
                    return res.send({ message: "Password is incorrect!" });
                }
            }
        })
}

// Route for user details

	 module.exports.getProfile = (req, res) => {
	 	//console.log(req.params.id);

         return UserAccount.findById(req.params.userAccountId).then(result =>{
            result.userAccountPassword = "***";
	 		res.send(result);
	})
	 }



module.exports.getUserDetails = (req, res) => {

    return UserAccount.findById(req.params.userAccountId).then(result => res.send(result));
}




/* 
Create Order

    */

/* 

module.exports.checkout = async (req, res) => {

    const userData = auth.decode(req.headers.authorization)


    if (userData.userAccountIsAdmin) {
        return res.send(`You can't order`);
    }
    else {
        let isUserUpdated = await UserAccount.findById(userData.userAccountId).then(user => {
            user.orders.push({
                requestOrdersTotalAmount: req.body.requestOrdersTotalAmount,
                requestOrdersProducts: req.body.requestOrdersProducts
            })

            // Save the updated user information in the database
            return user.save()
                .then(result => {
                    console.log(result);
                    res.send(result);
                })
                .catch(error => {
                    console.log(error);
                    return false;
                })
        })
        // console.log(isUserUpdated)
        for (let a = 0; a < req.body.requestOrdersProducts.length; a++) {

            let data = {
                userAccountId: userData.userAccountId,
                productItemId: req.body.requestOrdersProducts[a].productItemId,
                productItemName: req.body.requestOrdersProducts[a].productName,
                orderQuantity: req.body.requestOrdersProducts[a].orderQuantity
            }

            let isProductUpdated = await ProductItem.findById(data.productItemId).then(product => {
                product.orders.push({
                    userAccountId: data.userAccountId,
                    orderQuantity: req.body.orderQuantity
                })

                // Minus the stocks available by quantity from req.body
                product.stocks -= data.quantity;

                return product.save()
                    .then(result => {
                        console.log(result);
                        return true;
                    })
                    .catch(error => {
                        console.log(error);
                        return false;
                    })
            })
        }

        // Condition will check if the both "user" and "product" document have been updated.

        // (isUserUpdated && isProductUpdated) ? res.send(true) : res.send(false)
    }
}

 */
/* 

module.exports.checkout = async(req, res)=>{
    const userData = auth.decode(req.headers.authorization);

    let productInput = await ProductItem.findById(req.body.productItemId)
    .then(result=>{
        return result;
    });

    let orderBuyers={
        userAccountId: userData.userAccountId,
        requestOrdersTotalAmount: userData.requestOrdersTotalAmount,
        orderQuantity: req.body.orderQuantity

    };
    

    if (userData.userAccountIsAdmin)
    {
        res.send(`Admin is not allowed to order`);
    }
    else
    {
        var totalAmmount = await (productInput.requestOrdersTotalAmount*orders.quantity)
    }


}
 
 */
/* 

module.exports.checkout = async (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if (userData.userAccountIsAdmin)
{
    return res.send(`Admin can't place an order`);
}
else
{
        let isUserUpdated = await UserAccount.findById(userData.userAccountId).then(UserAccount => { 
            console.log(UserAccount);
            UserAccount.requestOrders.push({
            requestOrdersTotalAmount: req.body.requestOrdersTotalAmount
                //productItems: req.body.productItems
        })
            return UserAccount.save()
        .then(result=>{
            res.send(result);
        })
        .catch(error=>{
            return false;
        })
        })
    for (let a = 0; a < req.body.productItems.length;a++)
        {
            let productItemInput = {
                userAccountId: userData.userAccountId,
                productItemId: req.body.productItemInput[a].productItemId,
                productItemQuantity: req.body.productItemInput[a].productItemQuantity
            }

            let isProductItemUpdated = await ProductItem.findById(productItemInput.productItemId).then(product=>{
                product.requestOrders.push({
                    userAccountId: productItemInput.userAccountId,
                    productItemQuantity: req.body.productItemQuantity
            })

        productItemStocks -= productItemInput.productItemQuantity;

        return product.save()
        .then(result=>{
            return true;
        })
        .catch(error=>{
            return false;
        })
    })
    }
}
}

  */

/* 

module.exports.checkout = async (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    let productItemNme = await ProductItem.findById(req.body.productItemId).then(result => result.productItemName);

    let productItemInput = {
        userAccountId: userData.userAccountId,
        productItemId: req.body.productItemId
    }


    console.log(productItemInput);

    // a user is updated if we receive a "true" value
    let isUserUpdated = await UserAccount.findById(productItemInput.UserAccount)
        .then(user => {
            user.requestOrders.push({
                productItemId: productItemInput.productItemId
            });

            // Save the updated user information in the database
            return user.save()
                .then(result => {
                    return true;
                })
                .catch(error => {
                    return false;
                })
        })

    console.log(isUserUpdated);

    let isProductItemUpdated = await ProductItem.findById(productItemNme.productItemId).then(product => {

        product.requestOrders.push({
            userAccountId: productItemInput.userAccountId
        })

        // Minus the slots available by 1
        product.productItemStocks -= 1;

        return course.save()
            .then(result => {
                return true;
            })
            .catch(error => {
                return false;
            })
    })

    console.log(isProductItemUpdated);

    // Condition will check if the both "user" and "course" document have been updated.
    // ternary operator
    (isUserUpdated && isProductItemUpdated) ? res.send(true) : res.send(false)

}
 */